#!/bin/bash

hostname=$(hostname)
node_number=$(echo "$hostname" | grep -o '[0-9]*$')
ha_group="HA-$node_number"
echo "$ha_group"
cd ..
directory="/etc/pve/nodes/srv-proxmox04/qemu-server"

inotifywait -m -e moved_to "$directory" | while read -r directory event file; do
    echo "Changement détecté dans $directory : $event - $file"
    vm_id=$(basename "$file" .conf | grep -oE '[0-9]+')
    echo "Le VM ID est : $vm_id"
    ha-manager add vm:$vm_id --group "$ha_group" --state started
    echo "machine added"
done


